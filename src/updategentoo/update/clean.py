# See LICENSE file for copyright and license details.
"""Clean module."""
import syslog

from updategentoo.message import message
from updategentoo.utils import run_command


COMMANDS = ["emerge --depclean", "eclean-pkg", "eclean-dist"]


def clean_system():
	"""Clean system."""

	syslog.syslog(syslog.LOG_INFO, "start clean packages")
	message.process("Clean system")
	for command in COMMANDS:
		status, _ = run_command(command.split())
		if status:
			message.error("Don't clean system")
			return -1
	message.notify("Clean the system")
	return 0
