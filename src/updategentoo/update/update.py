# See LICENSE file for copyright and license details.
"""Module to update system."""
import datetime
import json
import multiprocessing
import os
import re
import subprocess
import syslog
import time

from updategentoo.message import message
from updategentoo.options import config


MIN_AVAILABLE_RAM = 10
MIN_AVAILABLE_SWAP = 50

# RAM
RAM_FILE = "/proc/meminfo"

MEMINFO_RAM_TOTAL = "MemTotal:"
MEMINFO_RAM_AVAILABLE = "MemAvailable:"
MEMINFO_SWAP_TOTAL = "SwapTotal:"
MEMINFO_SWAP_FREE = "SwapFree:"

# Progres bar
BAR_SIZE = 20

# Packages times files
TMP_FILE_PACKAGE_TIMES = "/tmp/.updategentoo_packages.json"
FILE_PACKAGE_TIMES = "/var/lib/updategentoo/packages_times.json"
LIST_TIME_SIZE = 10


def update_system():
	"""Update system

	Use 2 threads.
		- main: Check ram.
		- thread: Update system.
	"""

	syslog.syslog(syslog.LOG_INFO, "start update packages")
	message.process("Update system")

	# Thread -> Update
	modeupdate = multiprocessing.Process(target=__updatesystem)
	modeupdate.start()

	# Check RAM all time
	ram_available = True
	while ram_available and modeupdate.is_alive():
		ram_available = __get_availableram()
		time.sleep(1)

	if not ram_available:
		# Kill all process
		modeupdate.terminate()
		syslog.syslog(syslog.LOG_ERR, "err: low ram available")
		message.error("Low RAM available")

		save_packages_times()
		return -1

	save_packages_times()
	return modeupdate.exitcode


def __updatesystem():
	"""Command to update system."""

	update = UpdateSystem()
	update.start()


class UpdateSystem():
	"""CLASS TODO"""

	package_installing = False

	total_packages = 0
	packages_time = {}

	current_package = []
	current_package_time_start = None
	update_time_start = None

	original_packages = None
	packages_to_install = []
	packages_estimate_time = {}

	# Progress
	progress_max_size = 0

	def start(self):
		"""Start update system"""
		# Make command
		command = (
			"emerge -vuDN --color n --with-bdeps=y @world "
			f"{config.config.emerge_opt}"
		)

		# Load package info
		self._load_packages_info()

		# Start emerge command
		return_status = 0
		return_error = ""
		with subprocess.Popen(
			command.split(),
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			universal_newlines=True
		) as process:

			message.step("Calculate packages...")
			update_status = 0
			for line in process.stdout:
				match update_status:
					case 0:
						# Calculate packages
						if line[0:7] == "Total: ":
							update_status = 1
						self.get_package_to_install(line.strip())
					case 1:
						# Estimate time packages to install
						self.get_install_time_estimate()
						update_status = 2
						message.step("Update packages...")
					case 2:
						# Compile and install
						self.emerge_package(line.strip())

			return_status = process.returncode
			if return_status:
				return_error = process.stderr

		message.notify("Update the system")

		# Finish
		if not return_status:
			return
		# Print error
		message.error("In update system")
		print(return_error)

	def get_package_to_install(self, line):
		"""Get time stimate from package to install."""

		# Package
		find_package = line.find("[ebuild")
		if find_package:
			return
		# Get package name and repo
		i = line.find("]") + 2
		j = line.find(" ", i)

		package_name = self._get_package_name(line[i:j])
		self.packages_to_install.append(package_name)

	def get_install_time_estimate(self):
		"""Estimate all time to install all and each package."""

		time_all = 0
		unknown_packages = 0
		for package in self.packages_to_install:
			# Exist old time package
			if package not in self.original_packages:
				unknown_packages += 1
				continue
			list_times = self.original_packages[package]
			time_avg = sum(list_times)/len(list_times)
			time_all += time_avg
			self.packages_estimate_time[package] = int(time_avg)

		time_all = str(datetime.timedelta(seconds=int(time_all)))
		message_time = ""
		if unknown_packages:
			message_time = (
				f"< {time_all} (unknown packages: {unknown_packages})"
			)
		else:
			message_time = f"= {time_all}"

		message_time = (
			f"Update time estimate (s): {message_time}"
		)
		message.message(message_time)
		message.notify(message_time)

	def emerge_package(self, line):
		"""Class line to emerge."""

		find_start = line.find(">>> Emerging (")
		find_finish = line.find(">>> Completed ")

		if not find_start:
			self.package_start(line)
			return
		if not find_finish:
			self.package_finish()
			return

	def package_start(self, line):
		"""Package start emerging."""
		# Get package name without version
		i = line.find(") ") + 2

		package_name = self._get_package_name(line[i::])

		self.current_package = package_name

		# Get progress
		i = line.find('(') + 1
		j = line.find('of', i) - 1
		k = line.find(')', j)
		current_number = int(line[i:j])
		total_number = int(line[(j + 4):k])

		self.package_installing = True

		# First package
		if current_number == 1:
			self.total_packages = total_number
			self.current_package_time_start = datetime.datetime.now()
			self.update_time_start = self.current_package_time_start
			if config.options.progress:
				print("\n\n")

		# Show time estimate only if progress option is enable
		if not config.options.progress:
			return

		time_package = "unknown"
		if package_name in self.packages_estimate_time:
			time_package = self.packages_estimate_time[package_name]
			time_package = str(datetime.timedelta(seconds=time_package))

		self.progress_max_size = progressbar(
			current_number,
			self.total_packages,
			package_name,
			time_package,
			self.progress_max_size,
		)

	def package_finish(self):
		"""Package finish install.

		Get the package time compiler and save all progress in tmp file.
		"""

		if not self.package_installing:
			return

		# Time to packge compile
		current_package_time_stop = datetime.datetime.now()
		current_package_time = (
			current_package_time_stop -
			self.current_package_time_start
		)
		package_seconds = int(current_package_time.total_seconds())

		self.packages_time[self.current_package] = package_seconds
		self.package_installing = False

		# Save time in tmp file
		with open(TMP_FILE_PACKAGE_TIMES, "w", encoding="utf-8") as outfile:
			json.dump(self.packages_time, outfile)

	def _get_package_name(self, line):
		"""Get package name and repo.

		Example
		-------

		Extract from `www-client/librewolf-135.0.1_p1:0/135::librewolf`:

			- `www-client/librewolf`
		"""
		temp = re.findall(r'-\d+', line)
		i = line.find(temp[0])
		package_name = line[0:i]

		return package_name

	def _load_packages_info(self):
		"""Load the old packages times."""

		# Check if directory exist
		file_dir = os.path.dirname(FILE_PACKAGE_TIMES)
		if not os.path.exists(file_dir):
			os.makedirs(file_dir)

		# Check if file exist
		if not os.path.isfile(FILE_PACKAGE_TIMES):
			with open(FILE_PACKAGE_TIMES, "w", encoding="utf-8") as outfile:
				outfile.write("{}")

		with open(FILE_PACKAGE_TIMES, 'r', encoding="utf-8") as openfile:
			self.original_packages = json.load(openfile)


def save_packages_times():
	"""Save all packges times.

	JSON struct:
		- package:
			- repository
			- list of times: max size 10
	"""

	# Check if tmp file exist
	if not os.path.isfile(TMP_FILE_PACKAGE_TIMES):
		return

	# Open files
	with open(FILE_PACKAGE_TIMES, 'r', encoding="utf-8") as openfile:
		packages_times = json.load(openfile)
	with open(TMP_FILE_PACKAGE_TIMES, 'r', encoding="utf-8") as openfile:
		tmp_packages_times = json.load(openfile)

	for package, package_info in tmp_packages_times.items():
		# New package
		if package not in packages_times:
			packages_times[package] = [package_info]
			continue

		# Add time to package
		list_times = packages_times[package]
		list_times.insert(0, package_info)
		if LIST_TIME_SIZE < len(list_times):
			list_times.pop()

	with open(FILE_PACKAGE_TIMES, "w", encoding="utf-8") as outfile:
		json.dump(packages_times, outfile)


def __get_availableram():
	"""Check current available ram.

	The conditions are the next.
	1. The min ram available is set in MIN_AVAILABLE_RAM .
	2. If swap options is enable. Then the min available RAM is the swap defined
		by MIN_AVAILABLE_SWAP, but only if RAM available is minor that
		MIN_AVAILABLE_RAM.
	"""

	# Check RAM
	mem_available_percentages = __get_ramvalue(
		MEMINFO_RAM_TOTAL,
		MEMINFO_RAM_AVAILABLE
	)
	if MIN_AVAILABLE_RAM <= mem_available_percentages:
		return True

	# SWAP option is disable
	if not config.options.swap:
		return False

	# Check SWAP
	mem_available_percentages = __get_ramvalue(
		MEMINFO_SWAP_TOTAL,
		MEMINFO_SWAP_FREE
	)
	if MIN_AVAILABLE_SWAP <= mem_available_percentages:
		return True
	return False


def __get_ramvalue(name_total, name_available):
	"""Get RAM percentage from total an available parameter value."""

	# Get RAM
	file = ""
	with open(RAM_FILE, encoding="utf-8") as reader:
		file = reader.read()
	# Get total RAM/SWAP
	i = file.find(name_total)
	i = i + len(name_total)
	j = file.find('\n', i) - 3
	mem_total = int(file[i:j])
	# Get used RAM/SWAP
	i = file.find(name_available)
	i = i + len(name_available)
	j = file.find('\n', i) - 3
	mem_available = int(file[i:j])

	return int(100 * mem_available / mem_total)


def progressbar(
	current_num,
	total_num,
	current_package,
	package_time_estimate,
	previus_max_size
):
	"""Print progress update bar."""

	str_prefix = "\033[1A\033[1A\033[1A\r"

	# Clean progress
	spaces = " " * (previus_max_size + 1)
	print(
		(
			f"{str_prefix}"
			f"{spaces}\n"
			f"{spaces}\n"
			f"{spaces}"
		),
		flush=True
	)

	# Update progress
	x = int(BAR_SIZE * current_num / total_num)
	str_bar = f"Current packages [{'#' * x}{'.' * (BAR_SIZE - x)}] "
	str_progress = f"{current_num}/{total_num}"
	spaces = " " * 10
	package_name = f"\nPackage: {current_package}"
	package_time = f"\nPackage time: {package_time_estimate}"

	print(
		(
			f"{str_prefix}{str_bar}{str_progress}{spaces}"
			f"{package_name}{spaces}"
			f"{package_time}{spaces}"
		),
		flush=True
	)

	# Get max size
	bar_size = len(str_progress) + len(str_bar)

	return max(bar_size, len(package_name))
