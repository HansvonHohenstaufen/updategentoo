# See LICENSE file for copyright and license details.
"""Module to sync repositories."""

import syslog

from datetime import datetime, timedelta

from updategentoo.message import message
from updategentoo.options import config
from updategentoo.utils import run_command


REPOSDIR = "/var/db/repos/"


def sync_repositories():
	"""Sync all repositories."""

	# Check sync enable
	if config.options.only_update:
		return 0

	# Get info
	status, repositories = __get_info()
	if status:
		return -1
	return __sync(repositories)


def __get_info():
	"""Get repositories names and timestamp."""

	message.process("Get repositories names")

	# Search repositories and timestamp
	status, info = run_command(["emerge", "--info"])
	if status:
		message.error("Don't get repositories info")
		return -1, []

	today = datetime.today().strftime("%Y%m%d")
	today = datetime.strptime(today, "%Y%m%d")

	# List repositories
	allrepos = []
	for line in info.splitlines():
		name = "Head commit of repository"
		i = line.find(name)
		if i < 0:
			continue
		# Get timestamp
		i = i + len(name)
		j = line.find(":", i)
		name = line[i + 1:j]
		allrepos.append(name)
		message.message(name)

	# Check if sync is available by timestamp
	message.process("Get repositories timestamps")

	repos = []
	for repo in allrepos:
		# Check if repository is used
		name = f"Timestamp of repository {repo}: "
		i = info.find(name)
		if i < 0:
			# Repository don't have timestamp
			repos.append(repo)
			continue

		# Get timestamp
		i = i + len(name)
		j = info.find("Head", i) - 1
		name = info[i:j]

		# Convert to day
		t = datetime.strptime(name, "%a, %d %b %Y %H:%M:%S %z") + timedelta(days=2)
		t = t.strftime("%Y%m%d")
		timestamp = datetime.strptime(t, "%Y%m%d")
		if timestamp < today:
			# Save
			repos.append(repo)

	if repos:
		return 0, repos
	message.warning("The last sync is too recent")
	return -2, []


def __sync(repositories):
	"""Sync the available repositories."""

	message.process("Sync repositories")
	for repo in repositories:
		syslog.syslog(syslog.LOG_INFO, f"sync '{repo}' repository")
		message.message(f"repository: {repo}")
		if __syncrepo(repo):
			syslog.syslog(syslog.LOG_ERR, f"err: don't sync '{repo}' repository")
			message.error(f"Don't sync: {repo}")
			return -1
	message.notify("Sync repositories")

	return 0


def __syncrepo(repo):
	"""Sync one repository."""

	command = f"emaint sync -r {repo}"
	status, info = run_command(command.split())
	if status:
		message.error(f"Error to sync {repo}")
		print(info.stderr.decode('utf8'))
		return -1

	return 0


__all__ = ['sync_repositories']
