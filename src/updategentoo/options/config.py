# See LICENSE file for copyright and license details.
"""Module to set configuration."""
from updategentoo.options.options import Options, Configuration


options = Options()
config = Configuration()
