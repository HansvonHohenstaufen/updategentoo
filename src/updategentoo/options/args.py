# See LICENSE file for copyright and license details.
"""Module to get args from command."""
import argparse
import multiprocessing
import os
from dataclasses import fields

from updategentoo.options import config
from updategentoo.options.options import Options


LIST_OPTIONS = {
	"--distcc": {
		"short": "-d",
		"help": "Enable distcc.",
		"type": int,
		"nargs": 2,
		"metavar": ("local", "remote"),
	},
	"--exclude": {
		"short": "-e",
		"help": "Exclude a package",
		"action": "append",
		"metavar": "package",
	},
	"--notify": {
		"short": "-n",
		"help": "Enable notifications",
		"action": "store_true",
	},
	"--only-sync": {
		"short": "-s",
		"help": "Only sync repositories",
		"action": "store_true",
	},
	"--only-update": {
		"short": "-u",
		"help": "Don't sync repositories",
		"action": "store_true",
	},
	"--progress": {
		"short": "-p",
		"help": "See progress",
		"action": "store_true",
	},
	"--swap": {
		"short": "-S",
		"help": "Use swap how limit to cancel all operation",
		"action": "store_true",
	},
	"--version": {
		"short": "-v",
		"help": "Print the version",
		"action": "store_true",
	},
}


class CustomArgumentParser(argparse.ArgumentParser):
	"""Replace help."""

	def format_help(self):
		help_text = super().format_help()
		return help_text.replace("options", "options")


def get_args():
	"""Get arguments."""

	message_usage = (
		"\n"
		"  %(prog)s [options]\n"
	)
	message_description = (
		"Updategentoo description"
	)

	# Create arguments
	parser = CustomArgumentParser(
		usage=message_usage,
		description=message_description
	)

	# Add options
	for opt, arg in LIST_OPTIONS.items():
		opt = [opt, arg.pop("short", None)]
		parser.add_argument(*opt, **arg)

	return vars(parser.parse_args())


def set_config(args):
	"""Get the current option."""

	# Set actions
	options_elements = {field.name for field in fields(Options)}
	args_options = {
		k: v for k, v in args.items() if k in options_elements
	}
	config.options = Options(
		**{
			key: args_options.get(key, False)
			for key in Options.__annotations__
		}
	)

	# Env
	config.config.env = dict(os.environ)

	# Options more complex
	if config.options.distcc:
		if __set_distcc(args["distcc"]):
			return -1
	if args["exclude"]:
		__set_exclude(args["exclude"])

	return 0


def __set_distcc(args):
	"""Check and set distcc cores.

	The ENV to emerge is.
		-j 2*(LOCAL_CORES*REMOTE_CORES)+1
		-l LOCAL_CORES
	"""

	local, remote = args

	local_cpu = multiprocessing.cpu_count()
	if (local <= 0) or (local_cpu < local):
		return -1
	if (remote <= 0) or (50 < remote):
		return -2

	remote = 2 * (remote + local) + 1

	makeopts = f"-j{remote} -l{local}"
	config.config.env |= {
		"FEATURES": "distcc",
		"MAKEOPTS": makeopts,
	}

	return 0


def __set_exclude(args):
	"""Add emerge options to exclude packages."""

	exclude = ""
	for pkg in args:
		exclude = f"{exclude} --exclude {pkg}"
	config.config.emerge_opt = f" {config.config.emerge_opt} {exclude}"


__all__ = ['get_args', 'set_config']
