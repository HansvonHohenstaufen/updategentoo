# See LICENSE file for copyright and license details.
"""Module to check current configuration."""
import os

from updategentoo.message import message
from updategentoo.options import config


def check_options():
	"""Check options.

	This options are prohibit:
		- Only root user can update the system.
		- Check the valid cores when use distcc,
		- Only sync and update simultaneous is a contradict.
	"""

	# Check root user
	if __check_user():
		message.error("Requires superuser access")
		return -1
	if __check_only():
		message.error("Don't valid only sync and update simulate option")
		return -3
	return 0


def __check_user():
	"""Check the current user is root."""

	user = os.environ.get('USER')
	if user != "root":
		return -1
	return 0


def __check_only():
	"""Is prohibit only sync and update simultaneous."""

	# Only one can be enable the same time
	if config.options.only_sync and config.options.only_update:
		return -1
	return 0


__all__ = ['check_options']
