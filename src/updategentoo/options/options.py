# See LICENSE file for copyright and license details.
"""Module to get configuration."""
from dataclasses import dataclass


@dataclass()
class Options:
	"""Available update Options.

	Options:
		distcc: Set DISTCC compiler.
		notify: Use notification by "notify".
		only_sync: Only sync repositories.
		progress: Print details from the current progress.
		swap: Add swap memory how limit from RAM usage.
		version: Print the version.
	"""

	distcc: bool = False
	notify: bool = False
	only_sync: bool = False
	only_update: bool = False
	progress: bool = False
	swap: bool = False
	version: bool = False


@dataclass()
class Configuration:
	"""Run configuration."""

	emerge_opt: str = ""
	env: dict = None
