# See LICENSE file for copyright and license details.
"""Module to print messages and notifications."""
from updategentoo.options import config
from updategentoo.utils import run_command


PREFIX = "Update Gentoo: "


class Message:
	"""Class to print all messages."""

	color = {
		"null": "\x1b[1;0m",
		"bold": "\x1b[01m",
		"Red": "\x1b[1;31m",
		"red": "\x1b[0;31m",
		"green": "\x1b[0;32m",
		"Green": "\x1b[1;32m",
		"yellow": "\x1b[33m",
		"cyan": "\x1b[36m"
	}

	def process(self, s):
		"""Start process."""
		print(f"{self.color['green']}[*] {s}{self.color['null']}")

	def step(self, s):
		"""Step."""
		print(f"{self.color['green']}[.] {self.color['null']}{s}")

	def message(self, s):
		"""Normal message."""
		print(s)

	def warning(self, s):
		"""Warning message."""
		print(f"{self.color['red']}[W] {s}{self.color['null']}")
		self.notify(f"Warning: {s}")

	def error(self, s):
		"""Error message."""
		print(f"{self.color['Red']}[E] {s}{self.color['null']}")
		self.notify(f"Error: {s}")

	def notify(self, s):
		"""Send a notificaction."""
		if not config.options.notify:
			return
		status, _ = run_command(["notify", f"{PREFIX} {s}"])
		if status:
			print(
				f"{self.color['Eed']}[E] "
				"Don't run notify"
				"{self.color['null']}"
			)


message = Message()
