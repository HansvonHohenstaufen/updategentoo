# See LICENSE file for copyright and license details.
"""Module to utils functions."""
import subprocess


def run_command(command, shell=False):
	"""Run a command"""

	try:
		info = subprocess.run(
			command,
			shell=shell,
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			check=True
		).stdout.decode('utf-8')
	except subprocess.CalledProcessError as e:
		return -1, e.output

	return 0, info
