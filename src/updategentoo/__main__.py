# See LICENSE file for copyright and license details.
"""Package entry point."""
import sys

from importlib.metadata import version

from updategentoo.message import message
from updategentoo.options import config
from updategentoo.options.args import get_args, set_config
from updategentoo.options.check import check_options
from updategentoo.update.sync import sync_repositories
from updategentoo.update.update import update_system
from updategentoo.update.clean import clean_system


def updategentoo_update():
	"""Update the system.

	1. Sync repositories.
	2. Update system.
	3. Clean system.
	"""
	# Sync repositories
	if sync_repositories():
		return -1
	if config.options.only_sync:
		return 0

	# Update system
	if update_system():
		return -2

	if clean_system():
		return -3
	return 0


def updategentoo_main():
	"""Check the args and update the system"""

	if set_config(get_args()):
		message.error("Error in distcc config")
		return -1

	if config.options.version:
		print(f"updategentoo-v{version('updategentoo')}")
		return 0

	if check_options():
		return -2

	if updategentoo_update():
		return -3

	return 0


def main():
	""""Entry point."""

	retval = updategentoo_main()
	sys.exit(retval)


if __name__ == "__main__":
	main()
